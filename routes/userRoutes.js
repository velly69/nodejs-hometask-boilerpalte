const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {
    res.json(UserService.getAll());
    next();
}, responseMiddleware);

router.get('/:id', (req, res) => {
    const found = UserService.search(item => item.id === req.params.id);
    if (found) {
        res.json(found);
        next();
    } else {
        res.status(400).json( {
            msg: `No member with the id of ${req.params.id}`
        });
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res) => {
    let newMember = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        password: req.body.password
    };
    newMember = UserService.createNew(newMember);
    if(!newMember ) {
        return res.status(400).json({
            msg: "You can't create a new one"
        })
    }
    res.json(UserService.getAll());
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res) => {
    const found = UserService.search(item => item.id === req.params.id);
    if (found) {
        let updateUser = req.body;
        UserService.getAll().forEach(user => {
            if(user.id === req.params.id){
                user.firstName = updateUser.firstName ? updateUser.firstName : req.body.firstName;
                user.lastName = updateUser.lastName ? updateUser.lastName : req.body.lastName;
                user.email = updateUser.email ? updateUser.email : req.body.email;
                user.phoneNumber = updateUser.phoneNumber ? updateUser.phoneNumber : req.body.phoneNumber;
                user.password = updateUser.password ? updateUser.password : req.body.password;
                user = UserService.updateOld(user.id, user);
                res.json({
                    message: 'User was updated', user
                });
        }
        });
        next();
    } else {
        res.status(400).json( {
            msg: `No user with the id of ${req.params.id}`
        });
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res) => {
    const found = UserService.search(item => item.id === req.params.id);

    if (found) {
        let fighters = UserService.getAll();
        UserService.removeUser(req.params.id);
        res.json({ msg : 'Member deleted', 
        fighters
        });
        next();
    } 
    else {
        res.status(400).json( {
            msg: `No member with the id of ${req.params.id}`
        });
        next();
    }
}, responseMiddleware);


module.exports = router;