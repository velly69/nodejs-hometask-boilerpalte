const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
           // TODO: Implement login action (get the user if it exist with entered credentials)
    const found = AuthService.login(item => item.email === req.params.email && item.password == req.params.password);
    if(found){
        res.data = data;
        res.status(200).json(found);
    }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);


module.exports = router;