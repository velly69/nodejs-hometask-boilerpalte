const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', (req, res, next) => {
    res.status(200).json(FighterService.getAll());
    next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    const found = FighterService.search(item => item.id === req.params.id);
    if (found) {
        res.status(200).json(found);
        next();
    } else {
        res.status(400).json( {
            error:true,
            msg: `No member with the id of ${req.params.id}`
        });
        next();
    }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    let newMember = {
        name: req.body.name,
        health: 100,
        power: req.body.power,
        defense: req.body.defense
    };
    newMember = FighterService.createNew(newMember);
    if(!newMember ) {
        res.status(400).json({
            error: true,
            msg: "You can't create a new one"
        })
        return next();
    }
    res.status(200).json(FighterService.getAll());
    next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    const found = FighterService.search(item => item.id === req.params.id);
    if (found) {
        let updFighter = req.body;
        FighterService.getAll().forEach(fighter => {
            if(fighter.id === req.params.id){
                fighter.name = updFighter.name ? updFighter.name : req.body.name;
                fighter.health = updFighter.health ? updFighter.health : req.body.health;
                fighter.power = updFighter.power ? updFighter.power : req.body.power;
                fighter.defense = updFighter.defense ? updFighter.defense : req.body.defense;
                fighter = FighterService.updateOld(fighter.id, fighter);
                res.status(200).json({
                    message: 'Fighter was updated', fighter
                });
        }
        });
        next();
    } else {
        res.status(400).json( {
            error: true,
            msg: `No fighter with the id of ${req.params.id}`
        })
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res) => {
    const found = FighterService.search(item => item.id === req.params.id);

    if (found) {
        let fighters = FighterService.getAll();
        FighterService.removeFighter(req.params.id);
        res.status(200).json({ msg : 'Member deleted', 
        fighters
        });
    } 
    else {
        res.status(400).json( {
            error: true,
            msg: `No member with the id of ${req.params.id}`
        })
    }
}, responseMiddleware);


module.exports = router;