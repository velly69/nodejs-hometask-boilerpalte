const { dbAdapter } = require('../config/db');
const { v4 } = require('uuid');


class BaseRepository {
    constructor(collectionName) {
        this.dbContext = dbAdapter.get(collectionName); //fighters/users
        this.collectionName = collectionName; 
    }

    generateId() {
        return v4(); //random int
    }

    getAll() {
        return this.dbContext.value(); //Array of users/fighters
    }

    getOne(search) {
        return this.dbContext.find(search).value(); //item of Array
    }

    create(data) { // parametr - ???
        data.id = this.generateId(); //random
        data.createdAt = new Date(); // date of creation
        const list = this.dbContext.push(data).write(); //add to array of users/fighters
        return list.find(it => it.id === data.id); //new element of Array
    }

    update(id, dataToUpdate) {
        dataToUpdate.updatedAt = new Date(); //the date of update
        return this.dbContext.find({ id }).assign(dataToUpdate).write(); //return updated item
    }

    delete(id) {
        return this.dbContext.remove({ id }).write(); //the array after removing
    }
}

exports.BaseRepository = BaseRepository;