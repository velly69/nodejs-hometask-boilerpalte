const { fighter } = require('../models/fighter');
const Joi = require('joi');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const data = req.body;
    const schema = Joi.object().keys({
        name: Joi.string().required(),
        power: Joi.number().min(1).max(100).positive().integer().required(),
        defense: Joi.number().min(1).max(10).positive().integer().required(), 
    });
    Joi.validate(data, schema, (err, value) => {
        if (err) {
            res.status(400).json({
                error: true,
                message: 'Invalid request data',
            });
            next('route');
        } else {
            req.body = data;
            next();
        }
    });
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const data = req.body;
    const schema = Joi.object().keys({
        name: Joi.string(),
        power: Joi.number().min(1).max(100).positive().integer(),
        defense: Joi.number().min(1).max(10).positive().integer(), 
    });
    Joi.validate(data, schema, (err, value) => {
        if (err) {
            res.status(400).json({
                error: true,
                message: 'Invalid request data',
            });
            next('route');
        } else {
            next();
        }
    });
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;