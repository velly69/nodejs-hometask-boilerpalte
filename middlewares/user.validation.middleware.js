const { user } = require('../models/user');
const Joi = require('joi');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const data = req.body;
    const schema = Joi.object().keys({
        firstName: Joi.string().required(),
        lastName:Joi.string().required(),
        email: Joi.string().email().required(), 
        phoneNumber: Joi.string().required(),
        password: Joi.string().min(3).required(), 
    });
    Joi.validate(data, schema, (err, value) => {
        if (err) {
            res.status(400).json({
                error: true,
                message: 'Invalid request data',
            });
            next('route');
        } else {
            req.body = data;
            next();
        }
    });
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const data = req.body;
    const schema = Joi.object().keys({
        firstName: Joi.string(),
        lastName:Joi.string(),
        email: Joi.string().email(), 
        phoneNumber: Joi.string(),
        password: Joi.string().min(3), 
    });
    Joi.validate(data, schema, (err, value) => {
        if (err) {
            res.status(400).json({
                error: true,
                message: 'Invalid request data',
            });
            next('route');
        } else {
            next();
        }
    });
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;