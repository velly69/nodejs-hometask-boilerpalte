const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getAll(){
        const items = FighterRepository.getAll(); 
        if(!items){
            return null;
        }
        return items;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    createNew(data){
        const newItem = FighterRepository.create(data);
        if(!newItem) {
            return null;
        }
        return newItem;
    }

    updateOld(id, dataToUpdate){
        const updatedItem = FighterRepository.update(id, dataToUpdate);
        if(!updatedItem){
            return null;
        }
        return updatedItem;
    }

    removeFighter(id){
        const removed = FighterRepository.delete(id);
        if(!removed){
            return null;
        }
        return removed;
    }
    // TODO: Implement methods to work with fighters
}

module.exports = new FighterService();