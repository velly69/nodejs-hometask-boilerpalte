const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    getAll(){
        const items = UserRepository.getAll();
        if(!items){
            return null;
        }
        return items;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    createNew(data){
        const newItem = UserRepository.create(data);
        if(!newItem) {
            return null;
        }
        return newItem;
    }

    updateOld(id, dataToUpdate){
        const updatedItem = UserRepository.update(id, dataToUpdate);
        if(!updatedItem){
            return null;
        }
        return updatedItem;
    }

    removeUser(id){
        const removed = UserRepository.delete(id);
        if(!removed){
            return null;
        }
        return removed;
    }
}

module.exports = new UserService();